package com.xnx3.wangmarket.plugin.bbs.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 论坛的最新动作
 * @author 管雷鸣
 *
 */
public class Action {

	public final static int MAX_NUMBER = 20;	//每个网站最多存储20条记录
	

	/**
	 * 论坛用户动作，动作的缓存，每个网站缓存最大20条动作记录
	 * map key: siteid 
	 * map value: String,即动作说明，使用html
	 */
	public static Map<Integer, List<String>> actionMap = null;
	static{
		actionMap = new HashMap<Integer, List<String>>();
	}
	
	/**
	 * 增加一个动作
	 * @param siteid
	 * @param text
	 */
	public static void addAction(int siteid, String text){
		List<String> list = actionMap.get(siteid);
		if(list == null){
			list = new ArrayList<String>();
		}
		list.add(text);
		//判断一下list中的条数，若是超过20条，那么删除过期的
		if(list.size() > 20){
			list.remove(list.size()-1);
		}
		
		actionMap.put(siteid, list);
	}
	
	/**
	 * 通过 siteid 获取该站点论坛的动作
	 * @param siteid 站点id
	 * @return 动作列表
	 */
	public static List<String> getActionList(int siteid){
		if(siteid == 0){
			return new ArrayList<String>();
		}
		
		List<String> list = actionMap.get(siteid);
		if(list == null){
			list = new ArrayList<String>();
		}
		actionMap.put(siteid, list);
		return list;
	}
}
