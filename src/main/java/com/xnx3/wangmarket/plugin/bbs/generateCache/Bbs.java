package com.xnx3.wangmarket.plugin.bbs.generateCache;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.j2ee.func.Log;
import com.xnx3.j2ee.generateCache.BaseGenerate;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.plugin.bbs.entity.PostClass;

/**
 *  论坛相关缓存。 这个不跟随项目启动而创建缓存，当论坛板块有修改时，才被动触发
 * @author 管雷鸣
 *
 */
public class Bbs extends BaseGenerate {
	
	/**
	 * 生成论坛板块数据缓存，数组存在
	 * @param site 当前登陆的站点用户，论坛版块所属的站点
	 * @param list 数据
	 */
	public void postClass(Site site, List<PostClass> list){
		if(site == null){
			return;
		}
		createCacheObject("postClass");
		for (int i = 0; i < list.size(); i++) {
			PostClass pc = list.get(i);
			cacheAdd(pc.getId(), pc.getName());
			Log.info(pc.toString());
		}
		this.addCommonJsFunction();
		AttachmentFile.putStringFile("site/"+site.getId()+"/data/plugin_bbs_postClass.js", this.content);
	}
	
	
}
