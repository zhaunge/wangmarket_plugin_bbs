<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../../iw/common/head.jsp">
	<jsp:param name="title" value="欢迎页面"/>
</jsp:include>

<div style="padding-top:20%; text-align:center; font-size:25px;">
	<h1>Hello， Welcome use BBS</h1>
</div>

<div style="padding: 60px;">
	<h2>使用方式:</h2>
	<div>
		首先，您需要先建立一个论坛板块。然后您的论坛就可以正常使用了！
		<br/>
		您的论坛访问网址为： 
		<div style="padding:10px;"><a href="/plugin/bbs/list.do?siteid=${site.id }" target="_black">点此打开论坛网址</a></div>
		使用时，例如，你可以将这个链接地址放到您的网站上，点击后直接进入论坛。
	</div>
</div>


</body>
</html>