<%@page import="com.xnx3.j2ee.util.Page"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- 这是日志列表页面底部的分页显示 -->
<div class="laypage-main">
	<!-- 判断当前页面是否是列表页第一页，若不是第一页，那会显示首页、上一页的按钮 -->
	<c:if test="${!page.currentFirstPage}">
		<a href="${page.firstPage }">首页</a>
		<a href="${page.upPage }">上一页</a>
	</c:if>
	<!-- 输出上几页的连接按钮 -->
	<c:forEach items="${page.upList}" var="a">
		<a href="${a.href }">${a.title }</a>
	</c:forEach>
	
	<!-- 当前页面，当前第几页 -->
	<span class="laypage-curr">${page.currentPageNumber }</span>
		
	<!-- 输出下几页的连接按钮 -->
	<c:forEach items="${page.nextList}" var="a">
		<a href="${a.href }">${a.title }</a>
	</c:forEach>	
	
	<!-- 判断当前页面是否是列表页最后一页，若不是最后一页，那会显示下一页、尾页的按钮 -->
	<c:if test="${!page.currentLastPage}">
		<a href="${page.nextPage }">下一页</a>
		<a href="${page.lastPage }">尾页</a>
	</c:if>
	
	<span>共${page.allRecordNumber }条，${page.currentPageNumber }/${page.lastPageNumber }页</span>
</div>
